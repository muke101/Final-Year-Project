#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "emissionfunctions.h"

double epsi = 10e-5;

double *arange(double a, double b, int x)    {   
    double *arr = malloc((size_t)(x+1)*sizeof(double));
    double step = (b-a)/x;
    int i;

    for (i=0; i <= x; i++)  {
        arr[i] = a+step*i;
    }   

    return arr;
}

double uniform(double a, double b)	{
	double range = b-a;
	return a + (rand()*range)/RAND_MAX;
}

int transform(double *u2, double *phi, double *z, double *z1, double *z2, double *uJakob, double *zJakob)	{

	double k, t, phi_k;

	k = uniform(0,1); t = uniform(0,1); phi_k = uniform(0,1);

	if (k < epsi || k > 1-epsi || t < epsi || t > 1-epsi)	return 0;

	*u2 = k/(1-k);
	*phi = phi_k*2.*M_PI;
	*z = t;
	*z1 = 1-pow(t,2);
	*z2 = pow(t,2);
	*uJakob = 1./(2.*k);
	*zJakob = 2.*t;

	return 1;
}

double ua2(double z, double phi, double u, double u2)	{
	return 1+2.*pow((1-z)/z,0.5)*u*cos(phi)+(1-z)/z*u2; 
}	

double ub2(double z, double phi, double u, double u2)	{
	return 1-2.*pow(z/(1-z),0.5)*u*cos(phi)+z/(1-z)*u2;  
}	

double fc_x(double z, double phi, double u, double u2, double x)	{
	return z*pow(ua2(z,phi,u,u2),1-x/2.)+(1-z)*pow(ub2(z,phi,u,u2),1-x/2.);
}

double fcCorrection(double z, double phi, double u2, double x)	{
	double u = pow(u2,0.5);
	return log(fc_x(z,phi,u,u2,x)); 
}

double nfEquation(double u2, double phi, double z, double correl, double uJakob)	{

	double u = pow(u2,0.5);
	double Hq = 1-((z*(1-z))/(1+u2))*pow(2.*cos(phi)+((1-2.*z)*u)/pow(z*(1-z),0.5),2);

	return uJakob*Hq*correl;
}

double caEquation(double u2, double phi, double z, double z1, double z2, double correl, double correlZ1, double correlZ2, double uJakob, double zJakob)	{ 

	double u = pow(u2,0.5);
	double Hg = -4+z*(1-z)/(1+u2)*pow(2.*cos(phi)+((1-2.*z)*u)/pow(z*(1-z),0.5),2);  
	double zCompOne = (1./(1-z1))*(1./2.+1./2.*(1-(1-z1)*u2/z1)/ua2(z1,phi,u,u2)+(1-z1*u2/(1-z1))/ub2(z1,phi,u,u2));
	double zCompTwo = (1./z2)*(1./2.+1./2.*(1-z2*u2/(1-z2))/ub2(z2,phi,u,u2)+(1-(1-z2)*u2/z2)/ua2(z2,phi,u,u2));

	return uJakob*(Hg*correl+zJakob*(zCompOne*correlZ1+zCompTwo*correlZ2));
}

double equation(const char *flag, double u2, double phi, double z, double z1, double z2, double correl, double correlZ1, double correlZ2, double uJakob, double zJakob)	{
	if (strcmp(flag, "nf") == 0)
		return nfEquation(u2, phi, z, correl, uJakob);
	if (strcmp(flag, "ca") == 0)
		return caEquation(u2, phi, z, z1, z2, correl, correlZ1, correlZ2, uJakob, zJakob);
	else	{
		printf("invalid flag\n");
		return 0;
	}
}

void fcorrelMC(const char *flag, double x, unsigned long long N, double *I, double *stddev)	{

	*I = 0;
	double r, I2 = 0;
	unsigned long long i;
	double u, u2, phi, z, z1, z2, fc, fcZ1, fcZ2, uJakob, zJakob;
	srand(time(0)); //seed RNG

	for (i=0; i < N; ++i)	{
		if (transform(&u2, &phi, &z, &z1, &z2, &uJakob, &zJakob) == 0) continue;
		fc = fcCorrection(z,phi,u2,x);
		fcZ1 = fcCorrection(z1,phi,u2,x);
		fcZ2 = fcCorrection(z2,phi,u2,x);
		r = equation(flag, u2, phi, z, z1, z2, fc, fcZ1, fcZ2, uJakob, zJakob);
		*I+=r/N;
		I2+=pow(r,2)/N;
	}

	double mu = *I/N;
	*stddev = pow((1./N)*(I2-2*mu*(*I)+N*pow(mu,2))/N, 0.5);
}

int main()	{
	int i;
	double stddev, I;
	double *x = arange(0,2,20);

	for (i=0; i <= 20; i++)	{
		fcorrelMC("nf", x[i], 10000000, &I, &stddev);
		printf("I: %f, stddev: %f\n", I, stddev);
	}

	free(x);

	return 0;
}
