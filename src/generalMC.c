#include "emissionfunctions.h"
#define ZETABUFFER 200

double generalZetaI(double *zetaPrev, double epsi, double R, double zetaSum, struct zetaI *leg1, struct zetaI *leg2)	{
	double r, phi;
	*zetaPrev = *zetaPrev*pow(uniform(0,1),1./R);
	if (*zetaPrev > epsi)	{
		r = uniform(0,1);
		phi = uniform(0,1)*2.*M_PI;
		if (r < 0.5)	{
			leg1->zeta = *zetaPrev;
			leg1->phi = phi;
			return generalZetaI(zetaPrev, epsi, R, zetaSum+(*zetaPrev), ++leg1, leg2);
		}
		else	{
			leg2->zeta = *zetaPrev;
			leg2->phi = phi;
			return generalZetaI(zetaPrev, epsi, R, zetaSum+(*zetaPrev), leg1, ++leg2);
		}
	}
	else	{
		return zetaSum;
	}
}

double modIZeta(struct zetaI *leg)	{
	double zeta, phi;
	double sinSum = 0, cosSum = 0;
	int i;

	for (i=0; leg[i].zeta; i++)	{
		zeta = leg[i].zeta;
		phi = leg[i].phi;
		sinSum+=zeta*sin(phi);
		cosSum+=zeta*cos(phi);
	}

	return pow(pow(sinSum,2) + pow(cosSum,2),0.5);
}

int genFirstEmission(double zeta, struct zetaI *leg1, struct zetaI *leg2)	{
	double r = uniform(0,1);
	double phi = uniform(0,1)*2.*M_PI;

	if (r < 0.5)	{
		leg1->zeta = zeta;
		leg1->phi = phi;
		return 0;
	}
	else	{
		leg2->zeta = zeta;
		leg2->phi = phi;
		return 1;
	}
}

void generalMC(double epsi, double R, unsigned long long N, double *I, double *stddev)	{
	*I = 0;
	srand(time(0));
	double zetaSum, obs, r, I2 = 0;
	double zeta; 
	unsigned long long i;
	int j, leg;
	struct zetaI leg1[ZETABUFFER];
	struct zetaI leg2[ZETABUFFER];
	for (i=0; i < N; i++)	{
		memset(leg1, 0, ZETABUFFER*sizeof(struct zetaI)); 
		memset(leg2, 0, ZETABUFFER*sizeof(struct zetaI));

		for (j=0; j < ZETABUFFER; j++)	{
			zeta = 1;
			leg = genFirstEmission(zeta, leg1, leg2);
			zetaSum = generalZetaI(&zeta, epsi, R, 0, leg ? leg1:leg1+1, leg ? leg2+1:leg2); //increment the required pointer depending on which holds the first emission
		}

		obs = (zetaSum + modIZeta(leg1) + modIZeta(leg2))/2.; 
		r = pow(0.5+obs, -R);

		*I+=r/N;
		I2+=pow(r,2)/N;
	}

	*stddev = pow((1./N)*(I2-pow(*I,2)),0.5);

}

