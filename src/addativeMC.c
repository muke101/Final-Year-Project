#include "emissionfunctions.h"

double fc(double u, double u2, double phi, double z, double x)	{
	return pow(1+u2,(x-1)/2)*fc_x(z, phi, u, u2, x);
}

double FcorrelMin(double fcorrel, double zetaV, double zetaSum, double R)	{
	return 1./zetaV*(exp(-R*log(zetaV*fcorrel+1+zetaSum))-exp(-R*log(zetaV+1+zetaSum)));
}

double FcorrelMaj(double fcorrel, double zetaSum, double R)	{
	return 1./R*(exp(-R*log(fcorrel+zetaSum))-exp(-R*log(1+zetaSum)));
}

double fcVsc(double zetaV, double zetaSum, double x, double R, double u2, double phi, double z)	{
	double u = pow(u2,0.5);
	return FcorrelMin(fc(u,u2,phi,z,x),zetaV,zetaSum,R)+FcorrelMaj(fc(u,u2,phi,z,x),zetaSum,R);
}

void totalMC(double x, double epsi, double R, unsigned long long N, double *I, double *stddev)	{

	unsigned long long i;
	double zetaSum, Cab, r, I2=0;
	double u2, phi, z, z1, z2, uJakob, zJakob, fc, fcZ1, fcZ2, zetaV;
	*I = 0;
	
	for (i=0; i < N; ++i)	{
		zetaV = uniform(0,1);
		if (transform(&u2, &phi, &z, &z1, &z2, &uJakob, &zJakob) == 0 || zetaV < epsi || zetaV > 1-epsi) continue;	
		zetaSum = iZeta(ZETA_0, epsi, R, 0);
		fc = fcVsc(zetaV,zetaSum,x,R,u2,phi,z);
		fcZ1 = fcVsc(zetaV,zetaSum,x,R,u2,phi,z1);
		fcZ2 = fcVsc(zetaV,zetaSum,x,R,u2,phi,z2);
		Cab = caEquation(u2,phi,z,z1,z2,fc,fcZ1,fcZ2,uJakob,zJakob)+nfEquation(u2,phi,z,fc,uJakob);
		r = Cab;
		*I += r/N; 
		I2 += pow(r,2)/N;
	}

	*stddev = pow((1./N)*(I2-pow(*I,2)),0.5);
}
