#ifndef EMISSIONFUNCTIONS_H_ 
#define EMISSIONFUNCTIONS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#define ZETA_0 1

struct zetaI	{
	double zeta;
	double phi;
};

double uniform(double a, double b);

double ua2(double z, double phi, double u, double u2);

double ub2(double z, double phi, double u, double u2);

double nfEquation(double u2, double phi, double z, double correl, double uJakob);

double caEquation(double u2, double phi, double z, double z1, double z2, double correl, double correlZ1, double correlZ2, double uJakob, double zJakob);

double iZeta(double zetaPrev, double epsi, double R, double zetaSum);

double fc_x(double z, double u, double u2, double phi, double x);

double fcCorrection(double z, double phi, double u2, double x);

double fcVsc(double zetaV, double zetaSum, double x, double R, double u2, double phi, double z);

int transform(double *u2, double  *phi, double *z, double *z1, double *z2, double *uJakob, double *zJakob); 

#endif
